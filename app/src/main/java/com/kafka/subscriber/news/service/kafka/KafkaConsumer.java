package com.kafka.subscriber.news.service.kafka;

/**
 * Created by Chira Paul on 3/7/2018.
 */

public interface KafkaConsumer {
    void subscribe();
}
