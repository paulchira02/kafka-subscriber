package com.kafka.subscriber.news.service;

import com.kafka.subscriber.news.service.kafka.KafkaConsumer;
import com.kafka.subscriber.news.service.kafka.KafkaConsumerImpl;

/**
 * Created by Chira Paul on 3/7/2018.
 */

public class KafkaService {

    private static KafkaService kafkaService = new KafkaService();
    private KafkaConsumer kafkaConsumer;

    private KafkaService() {
        this.kafkaConsumer = new KafkaConsumerImpl();
    }

    public KafkaService getKafkaService(){
        return kafkaService;
    }
}
